/*
Multisensor Sketch

Author: Thomas Krebs, thkrebs@gmx.de

This sketch reads temperature, humidity, light and presence.
It is based on the various examples sketches from SparkFun and MySensors 

It uses the following sensors present on Sensebender
Light           - BPW34
Temp&Humidity   - SI7021
Presence 

*/
#define MY_DEBUG_VERBOSE_SIGNING //!< Enable signing related debug prints to serial monitor
//#define MY_RF24_ENABLE_ENCRYPTION
//#define MY_DEBUG_VERBOSE_RF24
//#define MY_RF24_DATARATE RF24_1MBPS

#define MY_SIGNING_FEATURE
#define MY_SIGNING_ATSHA204
//#define MY_SIGNING_ATSHA204_PIN 17
#define MY_SIGNING_REQUEST_SIGNATURES
//#define MY_VERIFICATION_TIMEOUT_MS 10000
#define MY_RF24_CHANNEL 124

#define MY_DEBUG
#define MY_NODE_ID              33
#define MY_RADIO_NRF24
#define BATT_SENSOR

#include <MySensor.h>

#include <SPI.h>
#include <Wire.h>
#include <SI7021.h>
#include <avr/power.h>

#define VERSION           "1.0"
#define SKETCH_NAME       "Multisensor B"

#define TEMP_CHILD_ID   1
#define HUM_CHILD_ID    2
#define MOTION_CHILD_ID 4

// Uncomment the line below, to transmit battery voltage as a normal sensor value
#define BATT_SENSOR    199
#define MAX_VOLTAGE    4048                  // as measured    
#define MIN_VOLTAGE    2000                  // probably needs some calibration
const double ratio =   MAX_VOLTAGE / 2.75;   // 2.75 = 9.97/(9.97+4.725)*MAX_VOLTAGE

// How many milli seconds between each measurement
#define MEASURE_INTERVAL 120000

// FORCE_TRANSMIT_INTERVAL, this number of times of wakeup, the sensor is forced to report all values to the controller
#define FORCE_TRANSMIT_INTERVAL 30 

// When MEASURE_INTERVAL is 60000 and FORCE_TRANSMIT_INTERVAL is 30, we force a transmission every 30 minutes.
// Between the forced transmissions a tranmission will only occur if the measured value differs from the previous measurement

// HUMI_TRANSMIT_THRESHOLD tells how much the humidity should have changed since last time it was transmitted. Likewise with
// TEMP_TRANSMIT_THRESHOLD for temperature threshold.
#define HUMI_TRANSMIT_THRESHOLD   0.5
#define TEMP_TRANSMIT_THRESHOLD   0.5


// Pin definitions
#define LED_PIN            13            // TODO: need to check that
#define BATTERY_SENSE_PIN  A1            // select the input pin for the battery sense point
#define MOTION_SENSOR      3             // The digital input you attached your motion sensor.  (Only 2 and 3 generates interrupt!)
#define INTERRUPT          1             // Usually the interrupt = pin -2 (on uno/nano anyway)

// Global settings
int measureCount = 0;

int sendBattery = 0;
boolean isMetric = true;
boolean highfreq = true;
boolean motionDetected = false;
int repeats = 2;  // send repeats on failure

// Storage of old measurements
float lastTemperature = -100;
float lastHumidity = -100;
long  lastBattery = -100;
int   lastTripped = -1; 

int   motionTrips = 0;  // count the number of cont. motion trips

SI7021 humiditySensor;

// Sensor messages
MyMessage msgTemp(TEMP_CHILD_ID,V_TEMP);
MyMessage msgHum(HUM_CHILD_ID,V_HUM);
MyMessage msg(MOTION_CHILD_ID, V_TRIPPED);


#ifdef BATT_SENSOR
MyMessage msgBatt(BATT_SENSOR, V_VOLTAGE);
#endif


/****************************************************
 *
 * Setup code 
 *
 ****************************************************/
void setup()  
{ 
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  
  Serial.print(F(SKETCH_NAME));
  Serial.println(VERSION);
  Serial.flush();
  
  
#ifdef MY_SIGNING_ATSHA204_PIN
  // Make sure that ATSHA204 is not floating
  pinMode(MY_SIGNING_ATSHA204_PIN, INPUT);
  digitalWrite(MY_SIGNING_ATSHA204_PIN, HIGH);
#endif  
  
  // setup sensors
  humiditySensor.begin();  

  digitalWrite(LED_PIN, LOW);

  // setup motion sensor 
  pinMode(MOTION_SENSOR, INPUT);    
  Serial.println(F("Setup complete..."));
}


void presentation()  {
  // Send the sketch version information to the gateway and Controller
  sendSketchInfo(SKETCH_NAME, VERSION);

  // Present all sensors to controller
  present(TEMP_CHILD_ID, S_TEMP);
  present(HUM_CHILD_ID, S_HUM);
  present(MOTION_CHILD_ID, S_MOTION);


#ifdef BATT_SENSOR
  present(BATT_SENSOR, S_POWER);
#endif

  isMetric = getConfig().isMetric;
#ifdef MY_DEBUG
  Serial.print(F("isMetric: ")); Serial.println(isMetric);
#endif

  Serial.flush();
  Serial.println(F(" - Online!"));
}


/***********************************************
 *
 *  Main loop function
 *
 ***********************************************/
void loop()     
{ 
  measureCount++;
  sendBattery++;
  bool forceTransmit = false;

  if (motionDetected) {
    sendMotion(false);  // do not force transmission unless motion status has changed
    motionDetected = false;
  }
  
  // do not enter into sensor data gathering on each motion tripped
  if ((!motionDetected) || (motionTrips > 3)) {
    motionTrips = 0; // avoid starvation of temp measure in case we have a lot of motion
    
    if (measureCount > FORCE_TRANSMIT_INTERVAL) { // force a transmission
      forceTransmit = true; 
      measureCount = 0;
    }

    // I do the battery check at the beginning
    if (sendBattery > 60) {
       sendBattLevel(forceTransmit); // Not needed to send battery info that often
       sendBattery = 0;
    }
  
    // Get & send sensor data
    wait(200);
    sendTempHumidityMeasurements(forceTransmit);
    wait(200);
    sendMotion(forceTransmit);
  } 
  wait(100);  // I don't know whether that is really required; however I have the impression that shutting down the radio leads to
                 // problems in the communication when using signatures
  Serial.println("going to sleep");

  if (sleep(digitalPinToInterrupt(MOTION_SENSOR),RISING, MEASURE_INTERVAL)>0) {
    motionDetected = true;
    motionTrips++;
    Serial.print("Motion detected="); Serial.println(motionDetected);
  }

}

/*********************************************
 *
 * Sends state of motion sensor
 *
 * Parameters
 * - force : Forces transmission of a value (even if it's the same as previous measurement)
 *
 *********************************************/
void sendMotion(bool force) {
 
  bool tx = force;
    
  // Read digital motion value
  bool tripped = digitalRead(MOTION_SENSOR) == HIGH; 
  Serial.print(F("Tripped: ")); Serial.println(tripped);
  
  if (lastTripped != tripped) tx = true;
  if (tx) {
    resend(msg.set(tripped?"1":"0"),repeats);  // Send tripped value to gw 
    lastTripped = tripped;
  }
}

/*********************************************
 *
 * Sends temperature and humidity from Si7021 sensor
 *
 * Parameters
 * - force : Forces transmission of a value (even if it's the same as previous measurement)
 *
 *********************************************/
void sendTempHumidityMeasurements(bool force)
{
  bool tx = force;
  
  si7021_env data = humiditySensor.getHumidityAndTemperature();
  
  float diffTemp = abs(lastTemperature - (isMetric ? data.celsiusHundredths : data.fahrenheitHundredths)/100);
  float diffHum = abs(lastHumidity - data.humidityPercent);

  Serial.print(F("TempDiff :"));Serial.println(diffTemp);
  Serial.print(F("HumDiff  :"));Serial.println(diffHum); 

  if (isnan(diffHum)) tx = true; 
  if (diffTemp > TEMP_TRANSMIT_THRESHOLD) tx = true;
  if (diffHum >= HUMI_TRANSMIT_THRESHOLD) tx = true;

  if (tx) {
    float temperature = (isMetric ? data.celsiusHundredths : data.fahrenheitHundredths) / 100.0;
     
    int humidity = data.humidityPercent;
    Serial.print("T: ");Serial.println(temperature);
    Serial.print("H: ");Serial.println(humidity);
    
    resend(msgTemp.set(temperature,1),repeats);
    wait(20);
    resend(msgHum.set(humidity), repeats);
    lastTemperature = temperature;
    lastHumidity = humidity;
  }
}


/*********************************************
 * Prints error on I2C comm bus
 *********************************************/
void printError(byte error)
  // If there's an I2C error, this function will
  // print out an explanation.
{
  Serial.print(F("I2C error: "));
  Serial.print(error,DEC);
  Serial.print(F(", "));
  
  switch(error)
  {
    case 0:
      Serial.println(F("success"));
      break;
    case 1:
      Serial.println(F("data too long for transmit buffer"));
      break;
    case 2:
      Serial.println(F("received NACK on address (disconnected?)"));
      break;
    case 3:
      Serial.println(F("received NACK on data"));
      break;
    case 4:
      Serial.println(F("other error"));
      break;
    default:
      Serial.println(F("unknown error"));
  }
}


/********************************************
 *
 * Sends battery information (battery percentage)
 *
 * Parameters
 * - force : Forces transmission of a value
 *
 *******************************************/
void sendBattLevel(bool force)
{
  if (force) lastBattery = -1;
  long batteryV =  analogRead(BATTERY_SENSE_PIN);  
  for (int i = 1; i<5; i++) {
    long newSample = analogRead(BATTERY_SENSE_PIN); //readVcc();
    batteryV -= batteryV / (i+1);
    batteryV += newSample / (i+1);   
  }


  long vcc  = batteryV * (3.3 / 1024.0) * ratio;  // resolution of ADC pin
  if (vcc != lastBattery) {
    lastBattery = vcc;

#ifdef BATT_SENSOR
    resend(msgBatt.set(vcc),repeats);
#endif
    // Calculate on the fully charged cell. Since I have a step-up in place I go as low as possible no offset for minimum
   sendBatteryLevel(  ((vcc-MIN_VOLTAGE)*10.0)/((MAX_VOLTAGE-MIN_VOLTAGE)*10.0) *100.0);
  }
  Serial.print("vcc="); Serial.println(vcc);
}


/********************************************
 *
 * Send message, resend on error
 *
 * Parameters
 * - msg : message to send
 * - repeats: number of repetitions
 *
 *******************************************/
void resend(MyMessage &msg, int repeats)
{
  int repeat = 0;
  int repeatdelay = 0;
  boolean sendOK = false;

  while ((sendOK == false) and (repeat < repeats)) {
    if (send(msg)) {
      sendOK = true;
    } else {
      sendOK = false;
      Serial.print(F("Send ERROR "));
      Serial.println(repeat);
      repeatdelay += random(50,200);
    } 
    repeat++; 
    delay(repeatdelay);
  }
}

